-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2018 at 11:12 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sistem_inventaris_lab`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(11) NOT NULL,
  `nama_barang` varchar(100) NOT NULL,
  `merk_barang` varchar(100) NOT NULL,
  `jumlah_barang` int(100) NOT NULL,
  `stock_barang` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `nama_barang`, `merk_barang`, `jumlah_barang`, `stock_barang`) VALUES
(4, 'Keyboard', 'Komic', 30, 10),
(5, 'LCD Proyektor', 'Microvision ', 10, 5);

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id_event` int(11) NOT NULL,
  `nama_event` varchar(50) NOT NULL,
  `tanggal_event` date NOT NULL,
  `lokasi_event` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id_event`, `nama_event`, `tanggal_event`, `lokasi_event`) VALUES
(1, 'Seminar Android Dev Surabaya', '2018-11-27', 'Aula Smkn1 Surabaya'),
(2, 'Sertifikasi TOEFL ', '2018-11-25', 'Lab TKJ 101-106');

-- --------------------------------------------------------

--
-- Table structure for table `pinjam`
--

CREATE TABLE `pinjam` (
  `id_pinjam` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `jenis_peminjaman` enum('barang','ruang_lab') NOT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `id_ruanglab` int(11) DEFAULT NULL,
  `keperluan_pinjam` varchar(100) NOT NULL,
  `tanggal_mulai` datetime NOT NULL,
  `jam_ke` varchar(10) NOT NULL,
  `tanggal_kembali` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pinjam`
--

INSERT INTO `pinjam` (`id_pinjam`, `id_user`, `jenis_peminjaman`, `id_barang`, `id_ruanglab`, `keperluan_pinjam`, `tanggal_mulai`, `jam_ke`, `tanggal_kembali`) VALUES
(17, 7, 'barang', 5, NULL, 'Mengajar ', '2018-11-30 00:00:00', '2', '2018-12-01 00:00:00'),
(18, 7, 'barang', 4, NULL, '', '2018-11-29 00:00:00', '4', '2018-11-30 00:00:00'),
(19, 7, 'barang', NULL, 2, 'Mengajar ', '2018-11-30 00:00:00', '2', '2018-12-01 00:00:00'),
(32, 7, 'barang', 5, NULL, 'Mengajar ', '2018-11-30 00:00:00', '2', '2018-12-01 00:00:00'),
(33, 7, 'barang', NULL, 3, 'Mengajar ', '2018-11-30 00:00:00', '2', '2018-12-01 00:00:00'),
(34, 8, 'barang', 5, NULL, 'Mengajar ', '2018-11-30 00:00:00', '3', '2018-12-01 00:00:00'),
(35, 8, 'ruang_lab', NULL, 2, 'Mengajar ', '2018-11-30 00:00:00', '2', '2018-12-01 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ruang_lab`
--

CREATE TABLE `ruang_lab` (
  `id_ruanglab` int(11) NOT NULL,
  `nama_lab` varchar(100) NOT NULL,
  `kapasitas` int(32) NOT NULL,
  `jumlah_lab` int(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruang_lab`
--

INSERT INTO `ruang_lab` (`id_ruanglab`, `nama_lab`, `kapasitas`, `jumlah_lab`) VALUES
(2, 'Teknik Komputer dan Jaringan', 20, 3),
(3, 'Multimedia', 20, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nip` varchar(32) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `nama` varchar(32) NOT NULL,
  `email` varchar(32) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nip`, `username`, `password`, `nama`, `email`, `level`) VALUES
(1, '210210210', 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'admin', 'admin@gmail.com', 1),
(7, '2103157027', 'nia', '827ccb0eea8a706c4c34a16891f84e7b', 'nia kurniawati', 'nia@gmail', 2),
(8, '2103157029', 'ulfa', '827ccb0eea8a706c4c34a16891f84e7b', 'Ulfa Octa', 'ulfa@gmail.com', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id_event`);

--
-- Indexes for table `pinjam`
--
ALTER TABLE `pinjam`
  ADD PRIMARY KEY (`id_pinjam`),
  ADD KEY `id_anggota` (`id_user`),
  ADD KEY `id_barang` (`id_barang`),
  ADD KEY `id_ruanglab` (`id_ruanglab`);

--
-- Indexes for table `ruang_lab`
--
ALTER TABLE `ruang_lab`
  ADD PRIMARY KEY (`id_ruanglab`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id_event` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pinjam`
--
ALTER TABLE `pinjam`
  MODIFY `id_pinjam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `ruang_lab`
--
ALTER TABLE `ruang_lab`
  MODIFY `id_ruanglab` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pinjam`
--
ALTER TABLE `pinjam`
  ADD CONSTRAINT `pinjam_ibfk_3` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`),
  ADD CONSTRAINT `pinjam_ibfk_4` FOREIGN KEY (`id_ruanglab`) REFERENCES `ruang_lab` (`id_ruanglab`),
  ADD CONSTRAINT `pinjam_ibfk_5` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
