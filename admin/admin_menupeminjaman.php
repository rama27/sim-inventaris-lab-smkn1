<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="Untitled.css" type="text/css"> </head>

<body>
  <nav class="navbar navbar-expand-md bg-primary navbar-dark">
    <a class="navbar-brand" href="#">Sistem Informasi
      <br>Lab SMKN1 Surabaya</a>
    <a class="navbar-brand" href="#">Welcome Admin</a>
    <div class="container">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar2SupportedContent" aria-controls="navbar2SupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
      <div class="collapse navbar-collapse text-center justify-content-end" id="navbar2SupportedContent">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="index_admin.php"><i class="fa d-inline fa-lg fa-home"></i> Beranda</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="admin_admin.php"><i class="fa d-inline fa-lg fa-user-secret"></i>&nbsp;Admin</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="anggota_anggota.php"><i class="fa d-inline fa-lg fa-users"></i>&nbsp;Anggota</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="admin_event.php"><i class="fa d-inline fa-lg fa-magic"></i>&nbsp;Event</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="admin_inventaris.php"><i class="fa d-inline fa-lg fa-cubes"></i> Inventaris</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="admin_ruanglab.php"><i class="fa d-inline fa-lg fa-cube"></i>&nbsp;Ruang Lab</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#"><i class="fa d-inline fa-lg fa-handshake-o"></i>&nbsp;Peminjaman</a>
          </li>
        </ul>
        <a href="logout.php"class="btn navbar-btn btn-primary ml-2 text-white"><i class="fa d-inline fa-lg fa-user-circle-o"></i>&nbsp;Logout</a>
      </div>
    </div>
  </nav>
  <div class="py-5 text-center bg-dark">
    <div class="container py-5">
      <div class="row">
        <div class="col-md-12">
          <h1 class="display-3 mb-4 text-primary">Peminjaman Inventaris &amp; Ruang Lab</h1>
          <p class="lead mb-5">Kelola peminjaman yang diajukan oleh user</p>
          <a href="admin_pengajuanpinjam.php" class="btn btn-lg btn-primary mx-1"><i class="fa fa-fw fa-list"></i>&nbsp;Lihat Daftar Pengajuan Peminjaman</a>
          <a href="admin_daftarpinjam.php" class="btn btn-lg btn-primary mx-1"><i class="fa fa-fw fa-handshake-o"></i>&nbsp;Lihat daftar Peminjaman</a>
        </div>
      </div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <pingendo onclick="window.open('https://pingendo.com/', '_blank')" style="cursor:pointer;position: fixed;bottom: 10px;right:10px;padding:4px;background-color: #00b0eb;border-radius: 8px; width:180px;display:flex;flex-direction:row;align-items:center;justify-content:center;font-size:14px;color:white">Made with Pingendo&nbsp;&nbsp;
    <img src="https://pingendo.com/site-assets/Pingendo_logo_big.png" class="d-block" alt="Pingendo logo" height="16">
  </pingendo>
</body>

</html>