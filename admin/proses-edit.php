<?php

include("koneksi.php");

// cek apakah tombol simpan sudah diklik atau blum?
if(isset($_POST['simpan'])){

    // ambil data dari formulir
    $id = $_POST['id'];
    $nip = $_POST['nip'];
    $username = $_POST['username'];
    $password = $_POST['passwd'];
    $nama = $_POST['nama'];
    $email = $_POST['email'];
    $level = $_POST['level'];
   

    // buat query update
    /*$sql = "UPDATE user SET nip='".$nip."',username='".$username."',password='".$password."',nama='".$nama."', email='".$email."', level='".$level."' WHERE id_user = '".$id."' ";
    $query = mysqli_query($db, $sql);*/

    //buat query update dengan method bind
    $sql = "UPDATE user SET nip = ? ,username = ?,password = ?,nama = ? , email= ? , level= ? WHERE id_user = ? ";
    $stmt = $db->prepare($sql);
    $stmt->bind_param('ssssssi', $nip, $username, $password, $nama, $email, $level, $id);
    $stmt->execute();

    // apakah query update berhasil?
    if( $stmt ) {
        // kalau berhasil alihkan ke halaman list-siswa.php
        header('Location: anggota_anggota.php');
        //var_dump($query);
    } else {
        // kalau gagal tampilkan pesan
        die("Gagal menyimpan perubahan...");
        
    }


} else {
    die("Akses dilarang...");
}

?>