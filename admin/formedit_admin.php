<?php

include("koneksi.php");

// kalau tidak ada id di query string
if( !isset($_GET['id']) ){
    header('Location: admin-admin.php');
}

//ambil id dari query string
$id = $_GET['id'];

// buat query untuk ambil data dari database
$sql = "SELECT * FROM user WHERE id_user=$id";
$query = mysqli_query($db, $sql);
$admin = mysqli_fetch_assoc($query);

// jika data yang di-edit tidak ditemukan
if( mysqli_num_rows($query) < 1 ){
    die("data tidak ditemukan...");
}

?>


<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="Untitled.css" type="text/css"> </head>

<body>
  <nav class="navbar navbar-expand-md bg-primary navbar-dark">
    <a class="navbar-brand" href="#">Sistem Informasi
      <br>Lab SMKN1 Surabaya</a>
    <a class="navbar-brand" href="#">Welcome Admin</a>
    <div class="container">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar2SupportedContent" aria-controls="navbar2SupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
      <div class="collapse navbar-collapse text-center justify-content-end" id="navbar2SupportedContent">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="#"><i class="fa d-inline fa-lg fa-home"></i> Beranda</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#" ><i class="fa d-inline fa-lg fa-user-secret"></i>&nbsp;Admin</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#"><i class="fa d-inline fa-lg fa-users"></i>&nbsp;Anggota</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#"><i class="fa d-inline fa-lg fa-magic"></i>&nbsp;Event</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#"><i class="fa d-inline fa-lg fa-cubes"></i> Inventaris</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#"><i class="fa d-inline fa-lg fa-cube"></i>&nbsp;Ruang Lab</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#"><i class="fa d-inline fa-lg fa-handshake-o"></i>&nbsp;Peminjaman</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="py-3">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1 class="">Edit Admin</h1>
        </div>
      </div>
    </div>
  </div>
  <div class="py-1">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <form action="proses-edit.php" method="post" class="">
            <input type="hidden" name="id" value="<?php echo $admin['id_user'] ?>">          
            <div class="form-group"> <label>NIP</label>
            <input type="text" name="nip" class="form-control" placeholder="Enter NIP" value="<?php echo $admin['nip'] ?>"> <small class="form-text text-muted"></small> </div>
            <div class="form-group"> <label>Username</label>
            <input type="text" name="username" class="form-control" placeholder="Enter Username" value="<?php echo $admin['username'] ?>"> </div>              
            <div class="form-group"> <label>Password</label>
            <input type="password" name="passwd" class="form-control" placeholder="Enter Password" value="<?php echo $admin['password'] ?>"> </div>
            <div class="form-group"> <label>Nama Admin</label>
            <input type="text" name="nama" class="form-control" placeholder="Enter Nama Admin" value="<?php echo $admin['nama'] ?>"> </div>
            <div class="form-group"> <label>Email Admin</label>
            <input type="email" name="email" class="form-control" placeholder="Enter Email Admin" value="<?php echo $admin['email'] ?>"> </div>
            <div class="form-group"> <label>Level</label>             
             <div class="radio">
               <label><input type="radio" name="level" value="1" checked>1</label>
             </div>
             </div>
            <button type="submit"name="simpan" class="btn btn-primary">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <pingendo onclick="window.open('https://pingendo.com/', '_blank')" style="cursor:pointer;position: fixed;bottom: 10px;right:10px;padding:4px;background-color: #00b0eb;border-radius: 8px; width:180px;display:flex;flex-direction:row;align-items:center;justify-content:center;font-size:14px;color:white">Made with Pingendo&nbsp;&nbsp;
    <img src="https://pingendo.com/site-assets/Pingendo_logo_big.png" class="d-block" alt="Pingendo logo" height="16">
  </pingendo>
</body>

</html>