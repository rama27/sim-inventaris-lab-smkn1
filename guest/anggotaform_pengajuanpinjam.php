<?php
include("koneksi.php");
session_start();

$sesi=$_SESSION['guest'];
// buat query untuk ambil data dari database
$sql = " SELECT * FROM user WHERE username='$sesi' ";
$query = mysqli_query($db, $sql);
$user = mysqli_fetch_assoc($query);
$_SESSION['id_user']=$user['id_user'];

// jika data yang di-edit tidak ditemukan
if( mysqli_num_rows($query) < 1 ){
    die("data tidak ditemukan...");
}
//echo mysqli_error($db);
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="Untitled.css" type="text/css"> </head>

<body class="bg-dark">
  <nav class="navbar navbar-expand-md bg-primary navbar-dark">
    <a class="navbar-brand" href="#">Sistem Informasi
      <br>Lab SMKN1 Surabaya</a>
    <a class="navbar-brand" href="#">Welcome Anggota</a>
    <div class="container">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar2SupportedContent" aria-controls="navbar2SupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
      <div class="collapse navbar-collapse text-center justify-content-end" id="navbar2SupportedContent">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="index_anggota.php"><i class="fa d-inline fa-lg fa-home"></i> Beranda</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="anggota_anggota.php"><i class="fa d-inline fa-lg fa-users"></i>&nbsp;Anggota</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="anggota_event.php"><i class="fa d-inline fa-lg fa-magic"></i>&nbsp;Event</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="anggota_inventaris.php"><i class="fa d-inline fa-lg fa-cubes"></i> Inventaris</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="anggota_ruanglab.php"><i class="fa d-inline fa-lg fa-cube"></i>&nbsp;Ruang Lab</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="anggotaform_pengajuanpinjam.php"><i class="fa d-inline fa-lg fa-handshake-o"></i>&nbsp;Peminjaman</a>
          </li>
        </ul>
        <a href="logout.php"class="btn navbar-btn btn-primary ml-2 text-white"><i class="fa d-inline fa-lg fa-user-circle-o"></i>&nbsp;Logout</a>
      </div>
    </div>
  </nav>
  <div class="py-3">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1 class="">Form Pengajuan Peminjaman</h1>
        </div>
      </div>
    </div>
  </div>
  <div class="py-1">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <form action="proses-pengajuanpinjam.php" method="post" class=""> 
            <input type="text" name="id" class="form-control" value="<?php echo $_SESSION['id_user']; ?>">
            <div class="form-group"> <label for="Jabatan">Jenis Peminjaman</label> <select name="jenispinjam" id="Jenis_Pinjam" class="form-control">
              <option value="barang">Barang</option><option value="ruang_lab">Ruang_Lab</option></select></div>
            <div class="form-group"> <label>Nama Barang</label>                    
             <select name="barang" id="Barang" class="form-control">
              <option value="NULL" name="barang">Not Insert</option>
              <?php $sql2 = "SELECT * FROM barang";
              $query2 = mysqli_query($db, $sql2);
              while($barang=mysqli_fetch_array($query2)){     
              echo "<option value=".$barang['id_barang'].">".$barang['nama_barang']."</option>"; }?>
              </select> 
              </div>
            <div class="form-group"> <label>Nama Ruang Lab</label>                    
             <select name="ruang" id="Ruang" class="form-control">
             <option value="NULL" name="ruang">Not Insert</option>
              <?php $sql3 = "SELECT * FROM ruang_lab";
              $query3 = mysqli_query($db, $sql3);
              while($ruang=mysqli_fetch_array($query3)){     
              echo "<option value=".$ruang['id_ruanglab'].">".$ruang['nama_lab']."</option>"; }?>
              </select> 
              </div>
            <div class="form-group"> <label>Keperluan Pinjam</label>
              <input type="text" name="keperluan" class="form-control" placeholder="Enter Keperluan"> </div>
            <div class="form-group inline"> <label>Tanggal Mulai Pinjam</label>
              <input type="date" name="tglmulai" class="form-control" placeholder="Enter Tanggal Mulai"> </div>
            <div class="form-group"> <label>Jam Ke</label>
              <input type="text" name="jamke" class="form-control" placeholder="Enter Jam Ke"> </div>
            <div class="form-group"> <label>Tanggal Kembali</label>
              <input type="date" name="tglkembali" class="form-control" placeholder="Tanggal Kembali"> </div>
            <button type="submit" name="tambah" class="btn btn-secondary">Tambah</button>
            <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
          </form>
        </div>
      </div>
    </div>
  </div>
  <pingendo onclick="window.open('https://pingendo.com/', '_blank')" style="cursor:pointer;position: fixed;bottom: 10px;right:10px;padding:4px;background-color: #00b0eb;border-radius: 8px; width:180px;display:flex;flex-direction:row;align-items:center;justify-content:center;font-size:14px;color:white">Made with Pingendo&nbsp;&nbsp;
    <img src="https://pingendo.com/site-assets/Pingendo_logo_big.png" class="d-block" alt="Pingendo logo" height="16">
  </pingendo>
</body>

</html>